FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get -y update && apt-get -y dist-upgrade

RUN apt install -y apt-transport-https ca-certificates curl software-properties-common screen

RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -

RUN add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"

RUN apt-get -y update && apt-get -y install git unzip osm2pgsql osmctools nano python-dev python3-gdal gdal-bin osm2pgrouting perl build-essential gcc python-dev libboost-all-dev cmake python3-sphinx python3-pip redis-tools

RUN apt-cache policy docker-ce

RUN apt install docker-ce -y

RUN pip3 install docker-compose

RUN mkdir /home/default && cd /home/default

RUN wget https://github.com/omniscale/imposm3/releases/download/v0.11.1/imposm-0.11.1-linux-x86-64.tar.gz

RUN tar -zxvf imposm-0.11.1-linux-x86-64.tar.gz && rm imposm-0.11.1-linux-x86-64.tar.gz && cd imposm-0.11.1-linux-x86-64

RUN ln -s ./imposm3 /usr/bin/imposm3

CMD exec /bin/bash -c "trap : TERM INT; sleep infinity & wait"
